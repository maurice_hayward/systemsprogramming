//Maurice Hayward 
//beetle.c

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <time.h>
#include "child.h"

#define PI 3.14159265359
#define RECSIZE 200.0
#define CANV_RAD 400.00/2.0
#define NANO 1000000000

double scale = 1;

extern int errno;

static char *colornames[] = {"red", "blue", "slateblue", "lightblue",
        "yellow", "orange", "gray90"
};


void print_usage(void){
	fprintf(stderr,"Usage: ./beetle SizeOfBoard NumberOfBeetles\n");
}

void parseinputs(int argc, char *argv[], long int * nrep, long int *N)
{
	
	char * ptr1;
	char * ptr2;

	//that that there are three arguments
	if(argc != 3){
        print_usage();
        exit(1); 
        }
	
	if (strncmp("./beetle", argv[0], strlen(argv[0])) != 0){
		print_usage();

      	exit(1); 
	}
	// Uses of errno to check overflow
	//source: http://linux.die.net/man/3/strtol
	errno = 0;
	*N = strtol(argv[1], &ptr1, 10);
	*nrep = strtol(argv[2], &ptr2, 10);

	if(errno == ERANGE){
		print_usage();
		exit(1);
	}


	if((*nrep <= 0) || (*N <= 0)  ){
		print_usage();
		exit(1);
	}
 
	if((*nrep <= 0) || (*N <= 0) || (*ptr1 != '\0') || (*ptr2 != '\0') ){
		print_usage();
		exit(1);
	}

	
}


//Generate a random variate thats between 0 and 1
//source: http://stackoverflow.com/questions/1340729/how-do-you-generate-a-random-double-uniformly-distributed-between-0-and-1-from-c
double Uniform(){
	return ((double)random()/(double)RAND_MAX);
}


//Finds a Uniform variate between a and b
//From  Simulations (CSCI 423) class library 
double U(double a, double b)
{ 
  return (a + (b - a) * Uniform());
}
static void sleeper(){
	//source:http://www.informit.com/articles/article.aspx?p=23618&seqNum=11
	
	double time_sec = 2.0 * scale ;
	struct timespec tv;
	tv.tv_sec = (int) time_sec;
	tv.tv_nsec = (long)((time_sec - tv.tv_sec)  * NANO);
	
	while(1){
		

		nanosleep(&tv,&tv);
		
		if(errno == EINTR){
			continue;
		}

		return;
	}
}

int handleselect(FILE * read_file){


    struct timeval timeout; 
    timeout.tv_sec = 0;
  	timeout.tv_usec = 0;

    fd_set master, read;

    int fd = fileno(read_file);
    FD_ZERO(&master);
    FD_ZERO(&read);
  	
    FD_SET(fd,&master);
     
    
    read = master;
    
    if((select(fd+1, &read, NULL, NULL, &timeout)) == -1){
      perror("bigbs:select");
      exit(1);
    }

    //puts("after select");
    int i;
    for(i = 0; i <=fd; i++){
        if(FD_ISSET(i, &read)){
          
          if(i == fd){
          	
            fscanf(read_file,"%lf", &scale);
           
        
        	}

          
    	}
    }

  return 1;
}


int main(int argc, char *argv[]){

	FILE *read_from, *write_to;
    char result[80], canvas_name[5];
    int childpid, i, totalTime;
	long int nrep; // number of replications
	long int N;       // size of the board 
	
	parseinputs(argc,  argv,  &nrep,  &N);
	
	const double bscale =  RECSIZE / (double)N;
	const double R = (double) (RECSIZE / 2.0); // max total displacement 
	const double offset = 4.0; double life_avg= 0.0;

	totalTime = 0;

	childpid = start_child("wish",&read_from,&write_to);
	/* Tell wish to read the init script */
	fprintf (write_to, "source beetle.tcl\n");

	for(i = 0; i < nrep; i++){
		handleselect(read_from);
		sleep(2);

		fprintf (write_to, ".c delete beetle\n");
		fprintf (write_to, ".c delete lines\n");
		double x =  CANV_RAD;
		double y = CANV_RAD;
		double timee = 0;
		fprintf (write_to, ".c itemconfigure beet_txt -text \"Beetle Number: %d \"\n", i + 1);
		fprintf (write_to, ".c create oval %d %d %d %d -fill %s -tags beetle\n", (int) (x   - offset) , (int) (y - offset), (int) (x  + offset), (int) (y  + offset), colornames[1]  );
		fprintf (write_to, "updates 0 \n");
		while(1){
		
			
			double rad = U(0,360) * (PI/180.0f);
			double lastx = x;
			double lasty = y;
			x = x + cos(rad)*bscale;
			y = y + sin(rad)*bscale;

			timee++;
			totalTime++;
			fprintf (write_to, "set lifetime \"%2.f\"\n", (double)timee);
			
			fprintf (write_to, "updates 0 \n");
			handleselect(read_from);
			sleeper();
			fprintf (write_to, ".c delete beetle\n");
			fprintf (write_to, ".c create line %d %d %d %d -arrow none -tags lines\n", (int)lastx, (int)lasty , (int)x , (int) y);
			if( ( x > CANV_RAD + R  - bscale) ||  (x < CANV_RAD - R + bscale) || (y < CANV_RAD - R + bscale) || ( y > CANV_RAD + R - bscale )){
				fprintf (write_to, ".c create oval %d %d %d %d -fill red -tags beetle\n", 
					(int) (x - offset) , (int) (y - offset), 
					(int) (x  + offset), (int) (y + offset)  );
			
			}
			else{
				fprintf (write_to, ".c create oval %d %d %d %d -fill blue -tags beetle\n", 
					(int) (x  - offset) , (int) (y  - offset), 
					(int) (x  + offset), (int) (y  + offset) );
			}

			if(( x > CANV_RAD + R ) ||  (x < CANV_RAD - R) || (y < CANV_RAD - R) || ( y > CANV_RAD + R )){
					fprintf (write_to, "set avg_life \"%f\" \n", (double)totalTime / (double)(i + 1.0));
					fprintf (write_to, "updates 0 \n");
					fprintf (write_to, "if {$sbuttontext == \"Silent\"} {bell}\n");
					break;
			}
			handleselect(read_from);
			timee++;
			totalTime++;
			fprintf (write_to, "set lifetime \"%2.f\" \n", (double)timee);
			//fprintf (write_to, "set avg_life \"%f\" \n", (double)totalTime / (double)(i + 1.0));
			fprintf (write_to, "updates 0\n");
		
	}


}

	//printf("%li by %li square, %li beetles, mean beetle lifetime is %.1f\n",N,N, nrep,(double) totalTime / nrep );
	return 0;
}



