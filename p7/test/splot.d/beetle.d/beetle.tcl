# splot.tcl

# Called whenever we replot the points

set sbuttontext "Silent"
set beetle_num 0
set avg_life 0.0
set lifetime  0.0
set life_txt  "Average Lifetime: $avg_life"
set Beetle_text "Beetle Number: $beetle_num"
set change_text 0

proc updates val {
    
    global change_text
    global life_txt
    global avg_life
    global lifetime

    if { $change_text == 1} \
    { set life_txt "Lifetime: $lifetime"} \
    else { set life_txt "Average Lifetime: $avg_life"}; \
    .c itemconfigure life -text $life_txt; 
}



# Create canvas widget
canvas .c -width 400 -height 400 -bg orange
pack .c 

#info text 
.c create text 0 0 -text $Beetle_text -anchor nw -justify left -tags beet_txt
.c create text 390 0 -text $life_txt -anchor ne -justify left -tags life



#create retangle  in canvas
.c create rectangle 100 100 300 300 -fill white  -outline black -width 3



# Frame for holding buttons
frame .bf
pack  .bf -expand 1 -fill x

# Exit button
button .bf.exit -text "Exit" -command {exit}


# Slient/Sound button
button .bf.sreset -text $sbuttontext -command \
    {if {$sbuttontext == "Silent"} {set sbuttontext "Sound"} else {set sbuttontext "Silent"}; \
      .bf.sreset configure -text $sbuttontext}
    


# Pack buttons into frame
pack .bf.exit .bf.sreset  -side left \
    -expand 1 -fill x

# Frame to hold scrollbars

frame .sf
pack  .sf -expand 1 -fill x

# Scrollbars for rotating view.  Call replot whenever

# Scrollbar for scaling view.
scale .sf.sscroll -label "Time Scaling" -length 450 \
  -from .1 -to 5 -orient horiz \
  -showvalue 1 -resolution .1
  .sf.sscroll set 1

button .sf.time -text "Time Scale"  -command { \
  puts stdout "[.sf.sscroll get]" }


# Pack them into the frame
pack .sf.sscroll .sf.time -side top



#Call replot



.c bind beet_txt <Button-1> { 
  if { $change_text == 1} \
    {set change_text 0; set life_txt "Average Lifetime: $avg_life" } \
    else {set change_text 1; set life_txt "Lifetime: $lifetime"}; \
    .c itemconfigure life -text $life_txt; 
  }



set beetle_num 34