#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <fcntl.h>
#include  <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

extern int errno;

#define READ 0
#define WRITE 1

//#define STDIN 0
//#define STDOUT 1

int pid;


int scan2even[2];
int even2scan[2];
int scan2odd[2];
int odd2scan[2];



int main(int argc, char *argv[], char *envp[]){

	
	
		//printf("%s\n", getenv("EODELAY"));
	

	//exit(0);
	if(argc != 2){
		puts("Usage: driver filepath");
		exit(1);
	}

	int fd;
	

	if (pipe(scan2even) == -1){
		perror("pipe");
		exit(1);
	}
	if (pipe(even2scan) == -1){
		perror("pipe");
		exit(1);
	}
	if (pipe(scan2odd) == -1){
		perror("pipe");
		exit(1);
	}
	if (pipe(odd2scan) == -1){
		perror("pipe");
		exit(1);
	}
	if((fd = open(argv[1], O_RDONLY)) == -1){
		printf("file %s doesn't exist\n", argv[1] );
		exit(1);
	}


	//fprintf(stderr,"%d\n", even2scan[READ]);

	if((pid = fork()) == 0){
		
		close(0);dup(fd);close(fd);
		close(scan2even[READ]);close(even2scan[WRITE]);
		close(scan2odd[READ]);close(odd2scan[WRITE]);

		int bufsize = 128;
		char *buf = (char *) malloc(bufsize);
		int n =0;
		
		if((n = snprintf(buf,bufsize,"%d %d %d %d", scan2even[WRITE], even2scan[READ], scan2odd[WRITE], odd2scan[READ])) >= bufsize){
			while(bufsize <= n){
				bufsize = bufsize*2;
			}
			buf  = (char *)realloc(buf,bufsize);
			snprintf(buf,bufsize,"%d %d %d %d", scan2even[WRITE], even2scan[READ], scan2odd[WRITE], odd2scan[READ]);
		}

		//printf("%s\n", buf);
		//exit(1);
		execlp("./scanner", "./scanner", buf, NULL);
		perror("execlp");
		_exit(1);
	}

	close(fd);
	close(scan2even[WRITE]); close(even2scan[READ]);
	close(scan2odd[WRITE]); close(odd2scan[READ]);
	
	if(fork() == 0){

		close(scan2odd[READ]);close(odd2scan[WRITE]);
		
		char  pids[50];
		if(sprintf(pids,"%d",pid) < 0){
			perror("sprintf");
			_exit(1);
		}

		close(0);dup(scan2even[READ]);close(scan2even[READ]);
		close(1);dup(even2scan[WRITE]);close(even2scan[WRITE]);
		execle("./even", "./even", pids, NULL,envp);
		perror("execlp");
		_exit(1);
	}

	if(fork() == 0){

		close(scan2even[READ]);close(even2scan[WRITE]);
		
		char  pids[50];
		if(sprintf(pids,"%d",pid) < 0){
			perror("sprintf");
			_exit(1);
		}

		close(0);dup(scan2odd[READ]);close(scan2odd[READ]);
		close(1);dup(odd2scan[WRITE]);close(odd2scan[WRITE]);
		execle("./odd", "./odd", pids, NULL,envp);
		perror("execlp");
		_exit(1);
	}

	close(scan2even[READ]); close(even2scan[WRITE]);
	close(scan2odd[READ]); close(odd2scan[WRITE]);
	
	
			


	wait(NULL);
	wait(NULL);
	wait(NULL);
		
	return 0;
}