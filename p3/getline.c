#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>


int getliner(int fd, char * buf, int * bufsize){
	char c = (char)0;
	int i = 0;

	while((read(fd,&c,1) > 0 ) && (c != '\n')){
		buf[i++] = c;

		if( (i + 1) >= *bufsize){
			*bufsize = *bufsize * 2;
			buf = (char *) realloc(buf, *bufsize);
		}
	}

	if(c == '\n'){
		buf[i] = c;
		++i;

		if( (i + 1) >= *bufsize){
			*bufsize = *bufsize * 2;
			buf = (char *) realloc(buf, *bufsize);
		}
	}

	buf[i] = '\0';
	return i;;

}


int main(void){

	int BUFFSIZE = 2;
	char *buf = (char *)malloc(BUFFSIZE);
	
	//char c;
	while( getliner(0,buf,&BUFFSIZE) > 0){
		printf("%s\n", buf);
		printf("%d\n", BUFFSIZE );
	}
	free(buf);
	return 0;
}