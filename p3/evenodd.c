#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

extern int errno;

int delay = 0;

char * REALLOC(char * b, int s){
	b = (char *) realloc(b,s);
	if(b == NULL){
		free(b);
		//printf(stderr,"%s\n", );
		exit(1);
	}

	return b;
}


int getword(int fd, char * buf, int * bufsize){
	//source: The C programing language 
	//Author: Kernigan and Ritchie
	char c = (char)0;
	int i = 0;
	//*buf = (char *)malloc(*bufsize);
	while((read(fd,&c,1) > 0 ) && (c != ' ') && (c != '\n')){
		buf[i++] = c;

		if( (i - 1) >= *bufsize){
			*bufsize = *bufsize * 2;
			//char * buftmp;
			buf = (char *) REALLOC(buf, *bufsize);
		}
	}

	if(c == ' ' || c == '\n'){
		buf[i++] = c;
	

		if( (i - 1) >= *bufsize){
			*bufsize = *bufsize * 2;
			buf = (char *) REALLOC(buf, *bufsize);
		}
	}

	buf[i] = '\0';
	return i;

}

typedef struct wnode{
	char * word;
	long int count;
	struct  wnode *  next;
	
}WNODE;

void print_usage(void){
	fprintf(stderr,"Usage1: ./even pid \n");
	fprintf(stderr,"Usage2: ./odd pid \n");
}

WNODE * walloc(char * word){
	WNODE * w = (WNODE *)malloc(sizeof(WNODE));
	w->word = strndup(word,strlen(word)+1);
	w->count = 1;
	w->next = NULL;

	return w;
}

void parseinputs(int argc, char *argv[], char *envp[], int * even , long int * pid){
	
	if( argc != 2 ){
		print_usage();
		exit(1);
	}
	
	if(!strncmp("./even", argv[0], 7) ){
				*even = 1;
			}
	else if(!strncmp("./odd", argv[0],7)){}
	else{
		print_usage();
		exit(1);
	}

	char * ptr, *ptr2;
	
	errno = 0;
	*pid = strtol(argv[1], &ptr, 10);

	if(errno == ERANGE){
		print_usage();
		exit(1);
	}


	if( (*pid < 0) || (*ptr != '\0') ){
		print_usage();
		exit(1);
	}


	if(getenv("EODELAY") == NULL){
		delay = 0;
		//fprintf(stderr,"delay = %d\n", delay);
		return;
	}
	errno = 0;
	delay = strtol(getenv("EODELAY"), &ptr2, 10);

	if(errno == ERANGE){
		delay = 0;	
	}

	if( (delay <= 0) || (*ptr2 != '\0') ){
		delay = 0;
	}

	//fprintf(stderr,"delay = %d\n", delay);

	

}

WNODE * insertNode(char * word, WNODE * HEAD){
//source:https://www.youtube.com/watch?v=ktjKKcgdh1I
//author: ReelLearning

	//case1: empty list
	if(HEAD == NULL){

		HEAD = walloc(word);
	}
	else{

		WNODE * curr = HEAD;
		WNODE * trail = NULL;

		//traerse list to find insert location
		while(curr != NULL){

			if(strcmp(curr->word, word) > 0){
				break;
			}
			else if(!strcmp(curr->word, word)){
				++curr->count;
				return HEAD;
			}
			else{
				
				trail = curr;
				curr = curr->next;
			}
		}

		//case 2- insert at head (not empty)
		WNODE * newNode = walloc(word);
		if(curr == HEAD){

			newNode->next = HEAD;
			HEAD = newNode;
		}
		else{
			//case3 - insert after head (not empty)
			newNode->next = curr;
			trail->next = newNode;
		}

	}

	return HEAD;

}
void printList(WNODE * head){
	WNODE * curr = head;
	int bufsize = 128;
	char *buf = (char *) malloc(bufsize);
	int n = 0;
	while(curr != NULL){
		//sprintf(buf,"%s %li\n", curr->word, curr->count);
		if(curr->word[strlen(curr->word) -1] == '\n'){
			curr->word[strlen(curr->word) -1] = '\0';
		}
		if((n = snprintf(buf,bufsize,"%-s %li\n", curr->word, curr->count)) >= bufsize){
			while(bufsize <= n){
				bufsize = bufsize*2;
			}
			buf  = (char *)REALLOC(buf,bufsize);
			snprintf(buf,bufsize,"%s %li\n", curr->word, curr->count);
		}
		//fprintf(stderr,"%s\n", buff);
		if(write(1,buf, strlen(buf)) == -1){
			perror("write");
			exit(1);
		}
		//puts("chicken  1");
		curr = curr->next;
	}

	close(1);
	free(buf);
}

void NUKE(WNODE * H){
	WNODE * curr = H;
	
	while(curr != NULL){
		WNODE * temp = NULL;
		temp = curr;
		curr = curr->next;
		free(temp->word);
		free(temp);
	}
}

WNODE * makeWordList(void){

	WNODE * HEAD = NULL;

	int bufsize = 1024;
	char * buf = malloc(bufsize);
	

	while( getword(0, buf, &bufsize) > 0 ){
		

		if(delay){
			//fprintf(stderr,"delay for %d secs\n", delay);
			sleep(delay);
		}
		HEAD = insertNode(buf,HEAD);
		
		
	}
	
	close(0);
	free(buf);
	

	return HEAD;

}



int main(int argc, char *argv[], char *envp[]){

	int even = 0;
	long int pid = 0;

	parseinputs(argc, argv, envp, &even , &pid);

	//printf("even: %d pid: %li\n", even, pid);

	WNODE * HEAD = makeWordList();
	
	if(even && !!pid){
		sleep(10);
		if(pid != getpid()){
			kill(pid,SIGTERM);
		}
		
	}

	printList(HEAD);

	NUKE(HEAD);



	return 0;
}