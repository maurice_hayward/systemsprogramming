#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <signal.h>

#define TO_EVEN  0
#define FROM_EVEN 1
#define TO_ODD   2
#define FROM_ODD 3

extern int errno;
 int flag = 1;

typedef struct wnode{
	char * word;
	long int count;
	struct  wnode *  next;
	
}WNODE;

void termsighander(){
	
	flag = 0;

}
void parseinputs(int argc, char *argv[], int * fd){
	
	if( argc != 2 ){
		puts("Scanner Needs two argments");
		exit(1);
	}
	
	

	if(sscanf(argv[1], "%d %d %d %d", &fd[0], &fd[1], &fd[2], &fd[3]) <= 0){
		perror("sscanf");
		exit(1);
	}

	

}



WNODE * walloc(char * word, int count){
	WNODE * w = (WNODE *)malloc(sizeof(WNODE));
	w->word = strndup(word,strlen(word)+1);
	w->count = count;
	w->next = NULL;
	return w;
}

int getliner(int fd, char * buf, int * bufsize){
	//source: The C programing language 
	//Author: Kernigan and Ritchie
	char c = (char)0;
	int i = 0;
	int n;

	while((n = read(fd,&c,1) > 0 ) && (c != '\n')){
		
		buf[i++] = c; 

		if( (i + 1) >= *bufsize){
			*bufsize = *bufsize * 2;
			buf = (char *) realloc(buf, *bufsize);
		}

	}

	if(c == '\n'){
		buf[i] = c;
		++i;

		if( (i + 1) >= *bufsize){
			*bufsize = *bufsize * 2;
			buf = (char *) realloc(buf, *bufsize);
		}
	}

	buf[i] = '\0';
	return i;

}
void printList(WNODE * head){
	WNODE * curr = head;
	while(curr != NULL){
		printf("   %-15s  %-li\n", curr->word, curr->count);
		curr = curr->next;
	}
}



void clean_word(char * word){
	int i ;

	
	for(i = 0; i < strlen(word); i++ ){
		
		word[i] = (char) tolower(word[i]);
		if(!isalnum(word[i])){
			//SOURCE:http://stackoverflow.com/questions/5457608/how-to-remove-a-character-from-a-string-in-c
			//AUTHOR:Jared Burrows
			memmove(&word[i], &word[i + 1], strlen(word) - i);
			i = i-1;
		}

	}

	word = (char *)realloc(word, strlen(word) + 2);
	word = strcat(word, " ");
	//word[strlen(word)] = '/0';


}

void readfile(int * fd){
	//char * word = NULL;

	char *word;
	while( scanf("%ms", &word) > 0 ){ /// read stdin from driver

		clean_word(word);
	
			
			
			
			if(*word == ' '){
				free(word);
				continue;
			}
		
			switch((strlen(word)-1)%2){

				case 0:
						if(write(fd[TO_EVEN],word,strlen(word)) < 0){
							perror("write");
							exit(1);
							}
						break;
				case 1:
						if(write(fd[TO_ODD],word,strlen(word)) < 0){
							perror("write");
							exit(1);
						}
						break;
				default:
					puts("unexpected value");
					exit(1);

			}
			
			free(word);
		//}
	
	}

	close(0);close(fd[TO_EVEN]);close(fd[TO_ODD]);
}


WNODE * readPipe(int fd){
	WNODE * HEAD = NULL;
	WNODE * curr = NULL;
	//puts("read file");;
	char * word = NULL;
	int n = 0;
	long int count = 0;
	int bufsize = 1024;
	char * buf = (char * )malloc(bufsize);
	while( (n = getliner(fd, buf, &bufsize)) > 0 ){
		
		
		if( sscanf(buf,"%ms %li\n",&word,&count) == -1){
			perror("sscanf");
			exit(1);
		}

		
		if(HEAD == NULL){
	 			curr = walloc(word, count);
	 			HEAD = curr;
	 		}
	 	else{
				
	 			curr->next = walloc(word,count);
				curr = curr->next;

	 		}
	 	
 		free(word);
	}
	
	close(fd);

	return HEAD;
}

int main(int argc, char *argv[]){
	int fd[4]; 

	parseinputs(argc, argv, fd);

	readfile(fd);

	signal(SIGTERM, termsighander);
	while(flag){sleep(1);char c = '*'; write(1,&c,1);}
	char c = '\n'; write(1,&c,1);
	
	WNODE * HEAD_EVEN = readPipe(fd[FROM_EVEN]);
	WNODE * HEAD_ODD = readPipe(fd[FROM_ODD]);
	
	puts("Words with even letters:");
	printList(HEAD_EVEN);
	puts("Words with odd letters:");
	printList(HEAD_ODD);



	return 0;
}