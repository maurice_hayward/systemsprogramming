//Maurice Hayward 
//beetle.c

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <errno.h>

#define PI 3.14159265359

extern int errno;

void print_usage(void){
	fprintf(stderr,"Usage: ./beetle SizeOfBoard NumberOfBeetles\n");
}

void parseinputs(int argc, char *argv[], long int * nrep, long int *N)
{
	
	char * ptr1;
	char * ptr2;

	//that that there are three arguments
	if(argc != 3){
        print_usage();
        exit(1); 
        }
	
	if (strncmp("./beetle", argv[0], strlen(argv[0])) != 0){
		print_usage();

      	exit(1); 
	}



	//check that there are no decimals
	/*if( (strstr(argv[1],".") != NULL) || (strstr(argv[2],".") != NULL)){ 
		print_usage();
		exit(1);
	}*/

	// Uses of errno to check overflow
	//source: http://linux.die.net/man/3/strtol
	errno = 0;
	*N = strtol(argv[1], &ptr1, 10);
	*nrep = strtol(argv[2], &ptr2, 10);

	if(errno == ERANGE){
		print_usage();
		exit(1);
	}


	if((*nrep <= 0) || (*N <= 0)  ){
		print_usage();
		exit(1);
	}
 
	if((*nrep <= 0) || (*N <= 0) || (*ptr1 != '\0') || (*ptr2 != '\0') ){
		print_usage();
		exit(1);
	}

	
}


//Generate a random variate thats between 0 and 1
//source: http://stackoverflow.com/questions/1340729/how-do-you-generate-a-random-double-uniformly-distributed-between-0-and-1-from-c
double Uniform(){
	return ((double)random()/(double)RAND_MAX);
}


//Finds a Uniform variate between a and b
//From  Simulations (CSCI 423) class library 
double U(double a, double b)
{ 
  return (a + (b - a) * Uniform());
}



int main(int argc, char *argv[]){

	long int nrep; // number of replications
	long int N;       // size of the board 
	
	parseinputs(argc,  argv,  &nrep,  &N);
	
	const double R = (double) N / 2.0; // max total displacement 

	long int  totalTime = 0;

	long int i; 
	for(i = 0; i < nrep; i++){
		

		double x = 0.0;
		double y = 0.0;
		

		while(1){
		
			
			
			double rad = U(0,360) * (PI/180.0f);

			x = x + cos(rad);
			y = y + sin(rad);

			totalTime++;
			
			if( (fabs(x) > R) || (fabs(y) > R)){break;}
		
			totalTime++;
		
	}


}

	printf("%li by %li square, %li beetles, mean beetle lifetime is %.1f\n",N,N, nrep,(double) totalTime / nrep );
	return 0;
}



