//Maurice Hayward 

#include <stdio.h>
#include <string.h> 
#include <stdlib.h>
#include <errno.h>
#include <limits.h>

#define MAX_FNAME 1026
#define MAX_LSIZE 1026
#define GREP_SIZE (MAX_FNAME +50) 


extern int errno;

typedef struct snode{
	char * saying;
	long int startline;
	struct  snode  * next;
	int USED;
}SNODE;

typedef struct result{
	char * fname;
	long int line;
	struct result  * next;
	struct result * prev;
}RNODE;


char *str_replace ( const char *string, const char *substr, const char *replacement ){
	//source: http://coding.debuntu.org/c-implementing-str_replace-replace-all-occurrences-substring
	//aurthor:chantra
      char *tok = NULL;
      char *newstr = NULL;
      char *oldstr = NULL;
      char *head = NULL;
     
      /* if either substr or replacement is NULL, duplicate string a let caller handle it */
      if ( substr == NULL || replacement == NULL ) return strdup (string);
      newstr = strdup (string);
      head = newstr;
      while ( (tok = strstr ( head, substr ))){
        oldstr = newstr;
        newstr = malloc ( strlen ( oldstr ) - strlen ( substr ) + strlen ( replacement ) + 1 );
        /*failed to alloc mem, free old string and return NULL */
        if ( newstr == NULL ){
          free (oldstr);
          return NULL;
        }
        memcpy ( newstr, oldstr, tok - oldstr );
        memcpy ( newstr + (tok - oldstr), replacement, strlen ( replacement ) );
        memcpy ( newstr + (tok - oldstr) + strlen( replacement ), tok + strlen ( substr ), strlen ( oldstr ) - strlen ( substr ) - ( tok - oldstr ) );
        memset ( newstr + strlen ( oldstr ) - strlen ( substr ) + strlen ( replacement ) , 0, 1 );
        /* move back head right after the last replacement */
        head = newstr + (tok - oldstr) + strlen( replacement );
        free (oldstr);
      }
      return newstr;
}



void print_usage(void){
	fprintf(stderr,"Usage: ./sayings [-b] [-h] saying\n");
}

char * reallocCat( char * say, char *add){
	say =  (char *) realloc(say, strlen(say) + strlen(add) + 1 );

	say = strcat(say, add);

	return say;
}

SNODE *salloc(void){
	SNODE * s;
	s = (SNODE *) malloc(sizeof(SNODE));
	s->saying = (char *) malloc(sizeof(char) + 1);
	*(s->saying) = '\0';
	s->USED = 0;
	s->next = NULL;
	return s;
}

RNODE *Ralloc(void){
	RNODE * r;
	r = (RNODE *) malloc(sizeof(RNODE));

	if(r == NULL){puts("r is null in Ralloc"); exit(1);}
	r->fname = (char *) malloc(sizeof(char)+1);
	*(r->fname) = '\0';
	r->next = NULL;
	r->prev = NULL;
	return r;
}

RNODE * parseGrep(int bflag, char * term){
	//puts("in parseGrep");
	RNODE * r;
	RNODE * head;
	head = Ralloc();
	r = head;

	char * token = (char *) malloc(GREP_SIZE * sizeof(char));
	char * line = (char *) malloc(GREP_SIZE * sizeof(char));
	char * saveptr = (char *) malloc(MAX_FNAME * sizeof(char));


	while((line = fgets(line, GREP_SIZE, stdin)) != NULL){

		
		token =  strtok(line, ":");
		
		
		if(token == NULL){fprintf(stderr,"Error while parsing  grep input\n");exit(1);}

		r->fname = strndup(token, MAX_FNAME);
		

		token =  strtok(NULL, ":");

		
		
		if(token == NULL){fprintf(stderr,"Error while parsing  grep input\n");exit(1);}

		
		errno = 0;
		r->line = strtol(token, &saveptr, 10);

	
		if(errno == ERANGE || r->line <= 0 || *saveptr != '\0'){fprintf(stderr,"Error while parsing  grep input\n");exit(1);}
		
		token =  strtok(NULL, ":");

		if(token == NULL){printf("%s",line);puts("4");print_usage();exit(1);}
		//memset(&token, 32, GREP_SIZE);
		//memset(&line, 32, GREP_SIZE);

		
		RNODE * next = Ralloc();
		r->next = next;
		r->next->prev = r;
		r = r->next;



	}

	if(r == head){
		if(*(r->fname) == '\0'){
			if(bflag){printf("\n0 witty sayings contain the string %s\n\n",  term); exit(0);}
			else{exit(0);}
		}
	}

	r->prev->next = NULL;
	
	free(r);
	//free(line);
	//free(token);
	//free(saveptr);
	//puts("out grep");

	return head;

}




SNODE * makeSayingsList(RNODE * r){

	FILE * fp;
	char * line;
	SNODE * s, *head;
	int linecount = 0;

	if( (fp = fopen(r->fname, "r")) == NULL){
		//puts("In Fopen");
		fprintf(stderr,"file: %s from grep input can't be found\n", r->fname);
		exit(1);
	}

	s = salloc();
	head = s;
	s->startline = 0;
	line  = (char *)malloc(1024);

	while( (line = fgets(line,1024,fp )) != NULL){
		linecount++;
		if(strncmp(line, "%\n",3) == 0){
			s->next = salloc();
			s = s->next;
			s->startline = linecount;
			continue;
		}
	
		s->saying = reallocCat(s->saying, line);
			
	}

	//s->next = salloc();
	//s = s->next;
	//s->startline = LONG_MAX;


	fclose(fp);
	return head;
}

	


void parseinputs(int argc, char *argv[], char **term, int * bflag, int *hflag){
	
	if( argc == 1 || argc > 4){
		print_usage();
		exit(1);
	}
	
	if(!strncmp("-b", argv[argc -1], 3) || !strncmp("-h", argv[argc -1], 3) ){
				print_usage();
				exit(1);
			} 

	int i;
	
	for( i = argc -2; i > 0; i-- ){
		int b = strncmp("-b", argv[i], 3);
		int h = strncmp("-h", argv[i], 3); 
		
		if( b !=0 &&  h !=0 ){
				print_usage();
				exit(1);
		}

		if(!b && *bflag){
			print_usage();
			exit(1);
		}

		if(!h && *hflag){
			print_usage();
			exit(1);
		}

		if(!b){
			*bflag = 1;
		}

		if(!h){
			*hflag = 1;
		}

	}

	*term = (char *) malloc( strlen(argv[argc -1]) + 1);
			
	strncpy(*term, argv[argc -1],strlen(argv[argc -1]) + 1 );

}
	
SNODE ** SayListofList(RNODE * RHEAD){
	
	long int index = 0;

	SNODE **SL = (SNODE **)malloc(sizeof(SNODE *));
	RNODE *curr = RHEAD;

	while( curr != NULL){
		if(curr == RHEAD){
			SL[(index)++] =  makeSayingsList(curr);
			curr = curr->next;
			continue;
		}

		if(strncmp(curr->fname, curr->prev->fname, MAX_FNAME) != 0){
			SL = (SNODE **)realloc(SL, (index +1) * sizeof(SNODE *) );
			SL[(index)++] = makeSayingsList(curr);
		}

		curr = curr->next;
	}

	(index)++;
	return SL;
}

void printSaying(SNODE * HEAD, RNODE * result, char ** output,  long int *count ){
	
	SNODE * curr = HEAD;
	
	while(curr->next != NULL){
		
		
		if((result->line > curr->startline) && (result->line < curr->next->startline)){
			if(curr->USED == 1)
				return;

			*output = reallocCat(*output, curr->saying);
			
			if(result->next != NULL){
				*output = reallocCat(*output, "------------------------------------\n");
			}

			curr->USED = 1;
			++(*count);
			return;
		}
		curr = curr->next;
	
	}

	fprintf(stderr, "No line: %li in grep file\n",result->line);
	exit(1);
}

void printALLsayings(SNODE ** SL, RNODE * RHEAD, long int bflag, long int hflag, char * term){
	long int count = 0;
	
	char * output;
	output = (char *)malloc(2*sizeof(char));
	*output = '\0';


	RNODE *curr = RHEAD;

	long int i = 0;
	while( curr != NULL){
		if(curr == RHEAD){

		}
		else if(strncmp(curr->fname, curr->prev->fname, MAX_FNAME) != 0){
			i++;
		}

		printSaying(SL[i],curr, &output, &count);


		curr = curr->next;

	}

	if(bflag){
		printf("\n%li witty sayings contain the string %s\n\n", count, term);
	}

	if(hflag){
		char *replace = (char *)malloc(strlen(term)+1+10);
 		sprintf(replace,"\e[7m%s\e[0m", term);
 		output = str_replace( output, term, replace );
	}

	printf("%s\n", output);

}


int main(int argc, char *argv[]){
	int bflag  = 0;
	int hflag = 0;
	char * term;
	
	parseinputs(argc,  argv, &term, &bflag, &hflag);

	//printf("term: %s hFlag: %d bflag: %d\n", term, hflag, bflag);

	//SNODE * sayings_head = makeSayingsList("work");
	RNODE * results_head = parseGrep( bflag, term);

	SNODE **SL = SayListofList(results_head);

	printALLsayings(SL, results_head, bflag,hflag, term);

	

	
	return 0;
}