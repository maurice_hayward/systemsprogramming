#include <stdio.h>
#include <pthread.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <errno.h>
#include <time.h>

#include "hostlist_cache.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define OVER "-1"
#define NANO 1000000000
#define SEC 0.001

extern char *optarg;
extern int optopt;
extern int optind;
extern int h_errno;
extern int errno;

extern int getopt(int argc, char * const argv[],const char *optstring);
extern void herror(const char *s);


static int BUFFER_SIZE;
static int CSIZE;
static int FDELAY;
static int TDELAY;

/* Circular buffer of integers. */

struct prodcons
{
  char ** buffer;	        /* the actual data */
  pthread_mutex_t lock;		/* mutex ensuring exclusive access to buffer */
  int readpos, writepos;	/* positions for reading and writing */
  pthread_cond_t notempty;	/* signaled when buffer is not empty */
  pthread_cond_t notfull;	/* signaled when buffer is not full */
};

static struct prodcons buffer;
static CACHE * cache;
static char  ** arguments;
static int arg_count;
static int full = 0;
static 	HNODE * HEAD = NULL;
static pthread_mutex_t cache_hostlist_lock = PTHREAD_MUTEX_INITIALIZER;
static int lines = 0;




/* Initialize a buffer */
static void init (struct prodcons *b, int BUFSIZE){
	int i;
	b->buffer = (char **)malloc(BUFSIZE*sizeof(char*));
	for(i = 0; i < BUFSIZE; i++){
		b->buffer[i] = malloc(21);
	}
	pthread_mutex_init (&b->lock, NULL);
  	pthread_cond_init (&b->notempty, NULL);
  	pthread_cond_init (&b->notfull, NULL);
  	b->readpos = 0;
  	b->writepos = 0;
}


void print_usage(void){
	puts("Usage: ./refstats -b numlines -N cachesize -d filedelay -D threaddelay file ...");
}

int string2int(char * arg){
	int num;
	char * word = NULL,  * ptr;
	errno = 0;

	word = strdup(arg);

	num = strtol(word, &ptr, 10);

	if( (errno == ERANGE) || (*ptr != '\0') || (num < 0) ){
		print_usage();
		exit(1);
	}

	free(word);
	return num;
}


void parseArguments(int argc, char* argv[]){

	int bflag = 0, Nflag = 0, dflag = 0, Dflag = 0;

	int option;
	int count = 0;

	if(argc < 10){
		print_usage(); 
        exit(1);
	}

	while ((option = getopt(argc, argv,"+b:N:d:D:")) != -1){
		count++;
		switch (option) {
             case 'b' : bflag = 1;
             			BUFFER_SIZE = string2int(optarg);
             			if(BUFFER_SIZE <= 0 || BUFFER_SIZE > 1000 ){
             				print_usage(); 
             				exit(1);
             			}
             			
                 break;
             case 'N' : Nflag = 1;
             			CSIZE = string2int(optarg);
             			if(CSIZE <= 0 || CSIZE > 10000 ){
             				print_usage(); 
             				exit(1);
             			}
             			
                 break;
             case 'd' : dflag = 1;
             			FDELAY = string2int(optarg);
             			
             	 break; 
             case 'D' : Dflag = 1;
             			TDELAY =  string2int(optarg);
             			
             	 break; 
             default: 
                    print_usage(); 
                    exit(1);
		}
	}

	if(!bflag||!Nflag||!dflag||!Dflag|| count != 4){
		print_usage(); 
        exit(1);
	}
}
void removeNewline(char * buffer){
	int len = strlen(buffer);
	if (len > 0 && buffer[len-1] == '\n')
    	buffer[len-1] = '\0';
}

void getHostname(char * IP){
	//source: http://beej.us/guide/bgnet/output/html/multipage/gethostbynameman.html
	struct hostent * hostinfo;
	struct in_addr ipv4addr;
	char * name;

	
	if ( inet_pton(AF_INET, IP, &ipv4addr) == 0){
		//printf("invalid\n");
		return;
	}

	pthread_mutex_lock(&cache_hostlist_lock);
	if ((name = inCache(cache, IP,  CSIZE )) == NULL){

		hostinfo =  gethostbyaddr(&ipv4addr,sizeof(ipv4addr), AF_INET);
		if(hostinfo == NULL){
			name = strdup(IP);
		}
		else{
			name = strdup(hostinfo->h_name);
		}

		
		insertCache(cache, IP, name, CSIZE);
	
	}
	
	HEAD = insertNode(HEAD, name);
	pthread_mutex_unlock(&cache_hostlist_lock);
	free(name);




}



static void sleeper(int milli){
	//source:http://www.informit.com/articles/article.aspx?p=23618&seqNum=11
	
	int time_sec = milli * SEC;
	struct timespec tv;
	tv.tv_sec = (time_t) time_sec;
	tv.tv_nsec = (long)(time_sec - tv.tv_sec)  * NANO;
	
	while(1){
		

		nanosleep(&tv,&tv);
		
		if(errno == EINTR){
			continue;
		}

		return;
	}
}
static void put (struct prodcons *b, char * data)
{
	pthread_mutex_lock (&b->lock);
	
	if(BUFFER_SIZE == 1){

		while(full){
			pthread_cond_wait (&b->notfull, &b->lock);
		}
		strncpy(b->buffer[0], data, 20);
		full = 1;

	}
	else{
	/* Wait until buffer is  full */
		while ((b->writepos + 1) % BUFFER_SIZE == b->readpos){
		  pthread_cond_wait (&b->notfull, &b->lock);
		  /* pthread_cond_wait reacquired b->lock before returning */
		}
		/* Write the data and advance write pointer */
		strncpy(b->buffer[b->writepos], data, 20);
		b->writepos++;
		if (b->writepos >= BUFFER_SIZE)
			b->writepos = 0;
		/* Signal that the buffer is now not empty */
		
	}

	pthread_cond_signal (&b->notempty);
	pthread_mutex_unlock (&b->lock);
}

static char* get (struct prodcons *b){
	char * data;
	pthread_mutex_lock(&b->lock);
	/* Wait until buffer is not empty */
	if(BUFFER_SIZE == 1){
		while(!full){
			
			pthread_cond_wait (&b->notempty, &b->lock);
		}

		data = strdup(b->buffer[0]);
		full = 0;
	}
	else{
		while (b->writepos == b->readpos){
			pthread_cond_wait (&b->notempty, &b->lock);
		}
		/* Read the data and advance read pointer */
	
		data = strdup(b->buffer[b->readpos]);
		
		b->readpos++;
		if (b->readpos >= BUFFER_SIZE)
			b->readpos = 0;
	
	
	}
	/* Signal that the buffer is now not full */
	pthread_cond_signal(&b->notfull);
	pthread_mutex_unlock(&b->lock);
	return data;
}
static void *producer (void *data){
	char buf[20];
  	int i;
	FILE * fp = NULL;
	i = optind;
	
	while(i < arg_count){
		
		if((fp = fopen(arguments[i], "r")) == NULL){
			
			perror("fopen:");
			i = i + 1;
			continue;
		}
		
		while( fgets(buf, sizeof(buf), fp) != NULL ){
			
			sleeper(FDELAY);
			lines++;
			removeNewline(buf);

			put(&buffer, buf);

		}
		
		i++;
		fclose(fp);
	}
	
	put (&buffer, OVER);
	put (&buffer, OVER);
	
  	return NULL;
}
static void *consumer (void *data){
	char *d;
	while (1)
	{
	  d = get (&buffer);
	  sleeper(TDELAY);
	  if (!strcmp(d, OVER)){break;}
	  //printf ("---> %s\n", d);
	  getHostname(d);
	  free(d);
	  
	}
	return NULL;
}

int main(int argc, char* argv[]){

	pthread_t th[3];
	void *retval;


	arguments = argv;
	arg_count = argc;
	parseArguments(argc, argv);

	//printf("NUMLINES:%d\nCSIZE:%d\nFDELAY:%d\nTDELAY:%d\noptind:%d\nArgc:%d\n"
		//, BUFFER_SIZE, CSIZE, FDELAY, TDELAY, optind, argc );

	init(&buffer,BUFFER_SIZE);
	
	cache = initCACHE( CSIZE);
	pthread_create (&th[0], NULL, producer, 0);
	pthread_create (&th[1], NULL, consumer, 0);
	pthread_create (&th[2], NULL, consumer, 0);
	

	pthread_join (th[0], &retval);
	pthread_join (th[1], &retval);
	pthread_join (th[2], &retval);

	
	printHostList(HEAD);

	if(!!lines)
		printf("\nCache ratio: %.2f\n", (float)cache->hit/lines );



	return 0;
}