#ifndef HOST_CACHE

#define HOST_CACHE

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

typedef struct hnode{
	char * name;
	unsigned int count;
	struct  hnode  * next;
}HNODE;

HNODE *halloc(char * name);
HNODE * insertNode(HNODE * HEAD,char * hostname);
void printHostList(HNODE * HEAD);

typedef  struct line{

	char * IP;
	char * hostname;
	unsigned int valid;
	unsigned int LastUsedCounter;

}LINE;


typedef struct cache{

	LINE * cachelines;

	unsigned int hit;

}CACHE;

CACHE * initCACHE( int csize );
void Evict(CACHE * C , int csize);
char * inCache(CACHE * C, char *ipaddress,  int csize );
void insertCache(CACHE * C, char *ipaddress, char * hostname,  int csize);

#endif  /* HOST_CACHE */
