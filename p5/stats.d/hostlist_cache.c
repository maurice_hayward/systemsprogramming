
#include "hostlist_cache.h"

static int test = 0;

HNODE *halloc(char * name){
	HNODE * h;
	h = (HNODE *) malloc(sizeof(HNODE));
	h->name = strdup(name);
	h->count = 1; 
	h->next = NULL; 
	return h;
}

HNODE * insertNode(HNODE * HEAD,char * hostname){
//source:https://www.youtube.com/watch?v=ktjKKcgdh1I
//author: ReelLearning

	//case1: empty list
	if(HEAD == NULL){

		HEAD = halloc(hostname);
	}
	else{

		HNODE * curr = HEAD;
		HNODE * trail = NULL;

		//traerse list to find insert location
		while(curr != NULL){

			if(strcmp(curr->name, hostname) > 0){
				break;
			}
			else if(!strcmp(curr->name, hostname)){
				++curr->count;
				return HEAD;
			}
			else{
				
				trail = curr;
				curr = curr->next;
			}
		}

		//case 2- insert at head (not empty)
		HNODE * newnode = halloc(hostname);
		if(curr == HEAD){

			newnode->next = HEAD;
			HEAD = newnode;
		}
		else{
			//case3 - insert after head (not empty)
			newnode->next = curr;
			trail->next = newnode;
		}

	}

	return HEAD;

}

void printHostList(HNODE * HEAD){
	printf("|---------------------------------------------------------------------|\n" );
	printf("|        hostname                                       |        count|\n");
	printf("|---------------------------------------------------------------------|\n" );
	//printf("%-20s|%12d|\n", );
	HNODE * curr = HEAD;
	while(curr != NULL){
		printf("|%-55s|%13d|\n", curr->name, curr->count);
		curr = curr->next;
	}

}

void printCache(CACHE * C, int csize, int flag){
	int i;

	if(!flag){
		puts("-------------");
		printf("CACHE BEFORE\n" );
	}
	else{
		printf("CACHE AFTER\n" );
	}
	
	for(i = 0; i < csize; i++){
		printf("cache[%d]: %s\n", i, C->cachelines[i].IP);
	}

	if(flag){
		puts("-------------");
	}
}



CACHE * initCACHE(int csize ){
	int i;

	CACHE * C = malloc(sizeof(CACHE));
	C->hit = 0;
	C->cachelines = (LINE*)malloc(csize * sizeof(LINE));

	for(i = 0; i < csize ; i++){
		
		C->cachelines[i].IP = NULL;
		C->cachelines[i].hostname = NULL;
		C->cachelines[i].valid = 0;
		C->cachelines[i].LastUsedCounter = 0;

	}
	
	return C;

}

void IncrementLastUsed(CACHE * C ,int indexofLasedUsed, int csize){
	int i;
	for(i = 0; i < csize; i++){
		++(C->cachelines[i].LastUsedCounter);
	}
	
	C->cachelines[indexofLasedUsed].LastUsedCounter = 0;
}

void Evict(CACHE * C , int csize){
	int i;
	int leastUsedIndex = 0;
	int max = 0;

	for(i = 0; i < csize; i++){
		if(C->cachelines[i].LastUsedCounter > max){
			leastUsedIndex = i;
			max = C->cachelines[i].LastUsedCounter;
		}
	}

	free(C->cachelines[leastUsedIndex].IP);
	free(C->cachelines[leastUsedIndex].hostname);
	C->cachelines[leastUsedIndex].valid = 0;
	C->cachelines[leastUsedIndex].IP = NULL;
	C->cachelines[leastUsedIndex].hostname = NULL;
	C->cachelines[leastUsedIndex].LastUsedCounter = 0;
}

char * inCache(CACHE * C, char *ipaddress,  int csize ){
	int i;
	if(test){
		
		printCache(C,csize, 0);
		printf("IP: %s\n",ipaddress );
	}
	//Check if there is a not valid line open to store data
	for(i=0; i < csize ; i++){
		if(C->cachelines[i].valid == 0){
			if(test){printf("MISS\n");}

			return NULL;
		}
		else if(!strcmp(ipaddress, C->cachelines[i].IP)){
			if(test){printf("HIT\n");}

			IncrementLastUsed(C ,i, csize);
			C->hit++;
			return strdup(C->cachelines[i].hostname);
		}
	}


	//If All Cache lines are valid and IP ADDRESS DON'T MATCH
	//We need to evict

	if(test){printf("MISS AND EVICTION\n");}

	Evict(C , csize);



	return NULL;

}

void insertCache(CACHE * C, char *ipaddress, char * hostname,  int csize){
	int i;

	for( i = 0; i < csize; i++){
		if(C->cachelines[i].valid == 0){

			C->cachelines[i].IP = strdup(ipaddress);
			C->cachelines[i].hostname = strdup(hostname);
			C->cachelines[i].valid = 1;
			IncrementLastUsed(C ,i, csize);
			if(test){printCache(C,csize, 1);}
			return;
		}


	}


	fprintf(stderr,"Something Wrong with CACHE!");
	exit(1);
}



