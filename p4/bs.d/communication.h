
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

void setupSock(struct sockaddr_in **addr, int* sock, int sockype, char *node,  char *port);
void printServerInfo( struct sockaddr_in * addr_s, struct sockaddr_in * addr_g,  int * sock_stream, int * sock_gram, socklen_t* len_s,socklen_t * len_g);
int sendall(int s, char *buf, int *len, struct sockaddr *to, socklen_t tolen );
int sendallBind(int s, char *buf, int *len);
int getword(int fd, char * buf, int * bufsize);
void getServerInfo(char ** node, char ** gameport, char ** qport);
void printsin(struct sockaddr_in *sin, char *host,char *m1);
int writeall(int sock, void *msg);
int writealls(int sock, char *msg);

 struct SHIPS {
    int  S1;
    int  S2;
  };