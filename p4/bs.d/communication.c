
#include "communication.h"


int writeall(int sock, void *msg){
	int left,put,num = 0;


	left = sizeof(msg); put=0;
  	while (left > 0){
    	if((num = write(sock, msg+put, left)) < 0) {
      		perror("inet_wstream:write");
      		return -1;
    	}
    	else left -= num;
    	put += num;
  	}

  	return 0;
}

int writealls(int sock, char *msg){
	int left,put,num;


	left = strlen(msg)+1; put=0;
  	while (left > 0){
    	if((num = write(sock, msg+put, left)) < 0) {
      		perror("inet_wstream:write");
      		return -1;
    	}
    	else left -= num;
    	put += num;
  	}

  	return 0;
}






int getword(int fd, char * buf, int * bufsize){
	//source: The C programing language 
	//Author: Kernigan and Ritchie
	char c = (char)0;
	int test;
	int i = 0;
	//*buf = (char *)malloc(*bufsize);
	while(((test = read(fd,&c,1)) > 0 ) && (c != ' ') && (c != '\n') && (c != '\0')){
		buf[i++] = c;

		if( (i - 1) >= *bufsize){
			*bufsize = *bufsize * 2;
			//char * buftmp;
			buf = (char *) realloc(buf, *bufsize);
		}
	}

	if( (i == 0) && (test < 0)){ return -1;}

	if(c == ' ' || c == '\n'){
		buf[i++] = c;
	

		if( (i - 1) >= *bufsize){
			*bufsize = *bufsize * 2;
			buf = (char *) realloc(buf, *bufsize);
		}
	}

	buf[i] = '\0';
	return i;

}

void printsin(struct sockaddr_in *sin, char *host,char *m1){
 

  printf ("%s", m1);
  printf ("  Server host =%s, port=%d\n",host,ntohs((unsigned short)(sin -> sin_port)));
}

int sendall(int s, char *buf, int *len, struct sockaddr *to, socklen_t tolen )
{
	//source:http://beej.us/guide/bgnet/output/html/multipage/advanced.html#sendall
	//AUTHOR:beej
    int total = 0;        // how many bytes we've sent
    int bytesleft = *len; // how many we have left to send
    int n;

    while(total < *len) {
        n = sendto(s, buf+total, bytesleft, 0, to, tolen );
        if (n == -1) { break; }
        total += n;
        bytesleft -= n;
    }

    *len = total; // return number actually sent here

    return n==-1?-1:0; // return -1 on failure, 0 on success
} 

int sendallBind(int s, char *buf, int *len)
{
	//source:http://beej.us/guide/bgnet/output/html/multipage/advanced.html#sendall
	//AUTHOR:beej
    int total = 0;        // how many bytes we've sent
    int bytesleft = *len; // how many we have left to send
    int n;

    while(total < *len) {
        n = send(s, buf+total, bytesleft, 0 );
        if (n == -1) { break; }
        total += n;
        bytesleft -= n;
    }

    *len = total; // return number actually sent here

    return n==-1?-1:0; // return -1 on failure, 0 on success
} 

void setupSock(struct sockaddr_in **addr, int* sock, int sockype, char *node,  char *port){
	int ecode;
	struct addrinfo hints, *addrlist;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET; hints.ai_socktype = sockype;
	hints.ai_flags = AI_NUMERICSERV | AI_PASSIVE; hints.ai_protocol = 0;
	hints.ai_canonname = NULL; hints.ai_addr = NULL;
	hints.ai_next = NULL;

	ecode = getaddrinfo(node, port, &hints, &addrlist);
	if (ecode != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ecode));
	    exit(1);
	  }

	*addr = (struct sockaddr_in *) addrlist->ai_addr;

	if ( (*sock = socket( addrlist->ai_family, addrlist->ai_socktype, 0 )) < 0 ) {
    	perror("commucation:socket");
   		exit(1);
	}

	


}
void printServerInfo( struct sockaddr_in * addr_s, struct sockaddr_in * addr_g,  int * sock_stream, int * sock_gram, socklen_t* len_s,socklen_t * len_g){
	char  myhostname[128];
	FILE * fp = NULL;
	unlink("./server");

	*len_s = sizeof(struct sockaddr_in);
  	if (getsockname(*sock_stream, (struct sockaddr *)addr_s, len_s) < 0) {
    	perror("inet_rstream:getsockname");
    	exit(1);
  	}

  	*len_g = sizeof(struct sockaddr_in);
  	if (getsockname(*sock_gram, (struct sockaddr *)addr_g, len_g) < 0) {
    	perror("inet_rstream:getsockname");
    	exit(1);
  	}

	if( (fp = fopen("server", "w+")) == NULL){
		fprintf(stderr,"comm: something wrond with filename server");
		exit(1);
	}

	if(gethostname(myhostname, 128) != 0) {
    	perror("comm:gethostname");
    	exit(1);
  	}

	  fprintf (fp,"%s\n%d\n%d\n",myhostname,ntohs((unsigned short)(addr_s -> sin_port)), ntohs((unsigned short)(addr_g -> sin_port)));

	  fclose(fp);
	
}

void getServerInfo(char ** node, char ** gameport, char ** qport){
	FILE * fp = NULL;

	if( (fp = fopen("server", "r")) == NULL){
		sleep(60);
		if( (fp = fopen("server", "r")) == NULL){
			fprintf(stderr,"comm: server doesn't exist\n");
			exit(1);
		}
	}

	fscanf(fp,"%ms\n",node);

	fscanf(fp,"%ms\n", gameport);
	fscanf(fp,"%ms\n", qport);

	
	fclose(fp);

}

 /*int main(){
	
	char * node = NULL, *gport = NULL,*qport =NULL;

	getServerInfo(node, gport, qport);

	
	socklen_t length;
	struct sockaddr_in *localaddr;
	int socket;
	

	setupSock(&localaddr, &socket, SOCK_STREAM, NULL, "0");

	if (bind(socket, (struct sockaddr *)localaddr, sizeof(struct sockaddr_in)) < 0) {
    	perror("myaddrs:bind");
   		exit(1);
  	}

	length = sizeof(struct sockaddr_in);
  	if (getsockname(socket, (struct sockaddr *)localaddr, &length) < 0) {
    	perror("inet_rstream:getsockname");
    	exit(1);
  	}
 	printServerInfo(localaddr, &socket, &length);
	
	return 0;
}*/