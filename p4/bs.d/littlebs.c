#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <sys/time.h>
#include <signal.h>
#include "communication.h"

extern char *optarg;
extern int errno;
int getopt(int argc, char * const argv[],const char *optstring);

static int qflag = 0;
static int tflag = 0;
static int pflag = 0;
static int timeout;
static char password[11] = {0};
static char HOME[25];  //intialize boards with spaces 
static char SHOTS[25];
static struct SHIPS S = {-1,-2};
static struct SHIPS OP = {-1,-2};
static int Aflag = 0;
static int game_sock;


void alarm_hand(){
	puts("timeout!");
	//write(game_sock, "0", 1 );
	exit(1);
}


void print_usage(void){
	puts("Usage: ./bs -q [-p <string>]");
    puts("or");
   	puts("Usage: ./bs  [-t <intger> -p <string>]");
}




void parseArguments(int argc, char* argv[]){

	

	int option;
	char * wait;

	while ((option = getopt(argc, argv,"qt:p:")) != -1){
		switch (option) {
             case 'q' : qflag = 1;
                 break;
             case 't' : wait  = strdup(optarg);
                        tflag = 1;
                 break;
             case 'p' :
             			if(strlen(optarg) > 10){
             				puts(" Password is at most 10 chars");
             				exit(0);
             			}
             			
             			strcpy(password, optarg);
             			password[10] = '\0';
             			pflag = 1;
             	 break; 
             default: 
                    print_usage(); 
                    exit(1);
		}
	}

	if(qflag && tflag){
		print_usage(); 
        exit(1);
	}

	if(pflag){printf("%s\n",password );}

	if(tflag){
		
		//timeout = (struct timeval *)malloc(sizeof(struct timeval));
		char * ptr;
		errno = 0;
		timeout = strtol(wait, &ptr, 10);
		
		if(errno == ERANGE){
			print_usage();
			exit(1);
		}


		if((timeout < 0) || (*ptr != '\0')){
			print_usage();
			exit(1);
		}

		

	}
	else{timeout = 0;}



}

void handleConnection(int sock, struct sockaddr * to ){
	char msg[11];
	char * handle;

	writealls(sock, password);

	if( (read(sock,msg, sizeof(msg))) < 0){perror("littlebs:read");exit(1);}

	

	if(!strcmp(msg, "D")){
		puts("Sorry the Server is DEAD");
		exit(0); 
	}
	if(!strcmp(msg, "f")){
		puts("Sorry the Server is busy managing a game");
		exit(0); 
	}
	else if(!strcmp(msg, "F")){
		puts("Sorry you need an password or your password is incorrect");
		exit(0);
	}
	else {
		
		printf("Your Player %s\n", msg);
		printf("Please Enter an handle (10 char max):");
		//fgets(msg, sizeof(msg), stdin);
		if(!strcmp(msg, "A")){ Aflag = 1;}
		

		
		scanf("%ms", &handle);
		handle[10] = '\0';

		
		writealls(sock, handle);
	}
	

}

void sendQuery(int sock, struct sockaddr_in *to){
	int len, cc;
	char msg[11];
	memset(msg, 0, sizeof(msg));

	socklen_t fsize = sizeof(struct sockaddr_in);

	if(pflag){
		strcpy(msg, password);
	}
	else{
		strcpy(msg, "q");
	}

	len = sizeof(msg); 
	if((sendall(sock, msg, &len, (struct sockaddr *)to, fsize)) < 0){
      perror("bigbs/handleQueue:sndall");
      fprintf(stderr,"We only sent %d bytes because of the error!\n", len);
      exit(1); 
    }
    
    alarm(10);
    memset(msg, 0, sizeof(msg));
    cc = recvfrom(sock,msg,sizeof(msg) ,0, (struct sockaddr *)to, &fsize);
    if(cc < 0) perror("littlebs:revcfrom");
    alarm(0);

    if(!strcmp(msg,"F")){
    	puts("Sorry Either you have no password or  an incorrect password");
    }
    else if(!strcmp(msg, "0")){
    	puts("There is no game in progress.");
    }
    else if(!strcmp(msg, "2")){
    	puts("There is a game in progress.");
    }
    else{
    	printf("There is no games in progress, but player %s is waiting for an opponent\n", msg );
    }


}

void initBoards(){
	memset(HOME, 32, 25);
	memset(SHOTS, 32, 25);

}
int notvaild(int pos ){
	return ((pos < 0) || (pos > 24));
}

int readKeyboard(){
	char * pos;
	int location;
	char * ptr;
	int n;


		
		n =  scanf("%ms", &pos);
		

		if(!strcmp(pos, "Q")){
			return (int)'Q';
		}
		errno = 0;
		location = strtol(pos, &ptr, 10);
	
	while((*ptr != '\0')|| (errno == ERANGE) || notvaild(location) || (n < 0) ){
		puts("Sorry that isn't a valid placement within [0,24]");
		printf("try again: ");
		n =  scanf("%ms", &pos);
		if(!strcmp(pos, "Q")){
			return (int)'Q';
		}
		errno = 0;
		location = strtol(pos, &ptr, 10);
	}

	free(pos);

	return location;
}
int  placeShip(){
	int ship = -1;

	ship =  readKeyboard();
	while(ntohl(S.S1) == ship){
		puts("Sorry that isn't a valid placement within [0,24]");
		printf("try again: ");
		ship =  readKeyboard();
	}


	HOME[ship] = 'S';

	return ship;
}

int placeShot(int sock, int p){
	
	if(OP.S1 == p){
		SHOTS[p] = '*';
		puts("YOU HIT A SHIP!");
		OP.S1 = -10;
	}
	else if(OP.S2 == p ){
		SHOTS[p] = '*';
		puts("YOU HIT A SHIP!");
		OP.S2 = -10;
	}
	else{
		SHOTS[p] = '@';
		puts("YOU MISSED :(");

	}

	if((OP.S1 == -10) && (OP.S2 == -10)){
		
		writealls(sock, "1");
		return -1;
	}
	writealls(sock, "0");
	
	return 1;

	
}

int acceptShot(int sock, int p){
	
	if(S.S1 == p){
		HOME[p] = '*';
		S.S1 = -10;
		puts("Bruh You Got HIT!");
	}
	else if(S.S2 == p){
		HOME[p] = '*';
		S.S2 = -10;
		puts("Bruh You Got HIT!");
	}else{
		HOME[p] = '@';
		puts("HAHA YOUR OPPONENT MISSED!");
	}

	if((S.S1 == -10) && (S.S2 == -10)){
		puts("YOU LOSE BRUH :(");	
		return -1;
	}

	return 1;	

}
void printBoards(){

	puts("   HOME           SHOTS   ");
	puts("+-+-+-+-+-+    +-+-+-+-+-+");

	int j;
	int i = 0;
	for(j = 0; j < 5; j++){

		int h;
		printf("|");
		for(h = 0; h < 5; h++){
			printf("%c|", HOME[i++]);

		}

		printf("    |");
		int s;

		i = i - 5;
		for(s = 0; s < 5; s++){
			printf("%c|", SHOTS[i++]);

		}

		printf("\n");
		puts("+-+-+-+-+-+    +-+-+-+-+-+");

	}
}

int main(int argc, char* argv[]){

	
	parseArguments(argc, argv);

	char * server_host = NULL, *gport =NULL, *qport = NULL;
	char msg[11];


	getServerInfo(&server_host,&gport,&qport);

	//socklen_t length;
	struct sockaddr_in *server_query, *server_game;
	int queue_sock;
	
	
	setupSock(&server_query, &queue_sock, SOCK_DGRAM, server_host, qport);
  	setupSock(&server_game, &game_sock, SOCK_STREAM, server_host, gport);


  	signal(SIGALRM, alarm_hand);
  	
  	if(qflag){
  		sendQuery(queue_sock, server_query);
  		exit(0);	
  	}

  	alarm(timeout);

  	if ( connect(game_sock, (struct sockaddr *)server_game, sizeof(struct sockaddr_in)) < 0) {
    	perror("littlebs:connect");
    	exit(1);
  	};
  	char Quit[4];
	sprintf(Quit, "%d", (int)'Q');
	
	printsin(server_game, server_host,"Connected to");
	printf("Please Wait.\n");
	
	memset(msg,0,sizeof(msg));

	handleConnection(game_sock, (struct sockaddr *)server_game );

	if( (read(game_sock,msg, sizeof(msg))) < 0){
		perror("littlebs:read");
		exit(1);
	}

	alarm(0);


	if(!strcmp(msg,Quit)){exit(0);}

	printf("Opponent's handle is %s\n\n", msg);
	
	
	initBoards();

	printf("Enter 1st ship location:");
	S.S1 = htonl(placeShip());

	printf("Enter 2nd ship location:");
	S.S2 = htonl(placeShip());
	printBoards();
	writeall(game_sock, (void *)&S);
	S.S1 = ntohl(S.S1);
	S.S2 = ntohl(S.S2);
	
	if( (read(game_sock,&OP, sizeof(OP))) < 0){perror("littlebs:read");exit(1);}
	OP.S1 = ntohl(OP.S1);
	OP.S2 = ntohl(OP.S2);
	

	int mybuf = 4, opbuf = 4;
  	char  *myshot = (char *)malloc(mybuf);
  	char  *opshot = (char *)malloc(opbuf);
  	memset(myshot, 0, sizeof(mybuf));
	memset(opshot, 0, sizeof(opbuf));
	
	while(1){

		if(Aflag){
			
			printf("Shoot into square ---> ");
			sprintf(myshot, "%d", readKeyboard());
			writealls(game_sock, myshot);
			if(!strcmp(myshot, Quit)){puts("QUITTER! YOU LOSE"); writealls(game_sock,"1");break;}
			if(placeShot(game_sock, strtol(myshot, NULL,10)) == -1){ puts("you WON!");break;}
			printBoards();
			if((getword(game_sock,opshot,&opbuf)) < 0){ perror("read"); exit(1);}
			if(!strcmp(opshot, Quit)){puts("YOU WIN! Your oppenent QUIT!");break;}
			if(acceptShot(game_sock, strtol(opshot, NULL, 10)) == -1){break;}	

		}
		else{
			
			if((getword(game_sock,opshot,&opbuf)) < 0){ perror("read"); exit(1);}

			if(!strcmp(opshot, Quit)){puts("YOU WIN! Your oppenent QUIT!");break;}
			if(acceptShot(game_sock, strtol(opshot, NULL, 10)) == -1){break;}
			printBoards();
			printf("Shoot into square ---> ");
			sprintf(myshot, "%d", readKeyboard());
			writealls(game_sock, myshot);
			if(!strcmp(myshot, Quit)){puts("QUITTER! YOU LOSE"); writealls(game_sock,"1");break;}
			if(placeShot(game_sock, strtol(myshot, NULL,10)) == -1){ puts("you WON!");break;}

		}	
		printBoards();

		memset(myshot, 0, sizeof(mybuf));
		memset(opshot, 0, sizeof(opbuf));

	}

	printBoards();


	exit(0);
}