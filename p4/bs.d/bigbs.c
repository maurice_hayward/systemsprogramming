#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <signal.h>
#include <errno.h>

#include "communication.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))

extern int errno;
static int players = 0;
static char handleA[11] ={0};
static char handleB[11] = {0};
static int playerA = 0;
static int playerB = 0;
static struct timeval timeout; 
static struct SHIPS shipsA = {0,0};
static struct SHIPS shipsB = {0,0};
static int pflag = 0;
static char pass[11] = {0};
//static SHIPS shipsA = {0, 0};
//static SHIPS shipsB = {0, 0};

void termsighander(){
  
  char Quit[4];
  sprintf(Quit, "%d", (int)'Q');
  unlink("server");
  fprintf(stderr, "HANDLE SIGHUP\n");
  switch(players){
    case 0: 
            break;
    case 1: writealls(playerA, Quit);
            break;
    case 2:
            writealls(playerA, Quit);
            writealls(playerB, Quit);

  }
 
  exit(0);
}

int handleQueueAndlistener(int listener, int queue, int flag);


void print_usage(void){
  puts("Usage: ./BS  [XXXXXXXXXX <string>]");
 
}

void resetGame(){
    close(playerA);
    close(playerB);
    memset(handleA, 0 , sizeof(handleA));
    memset(handleB, 0 , sizeof(handleB));
    memset(&shipsA, 0 , sizeof(shipsA));
    memset(&shipsB, 0 , sizeof(shipsB));
    playerA = 0;
    playerB = 0;
    players = 0;
}



void startGame( int listener, int queue){

  char msg[11];
  memset(msg, 0, sizeof(msg));


  puts("in game");
  handleQueueAndlistener(listener,queue, 0);

  if(*handleA == '\0'){resetGame();return;}
  if(*handleB == '\0'){resetGame();return;}


  printf("Player A is %s\n",handleA );
  printf("Player B is %s\n",handleB );

  //send player handles
  if(write(playerA, handleB, sizeof(handleB)) < 0){ fprintf(stderr, "-1\n");resetGame(); return;}
  if(write(playerB, handleA, sizeof(handleA)) < 0){ fprintf(stderr, "-1\n");resetGame(); return;}


 // while(1)



  //get ship positions
  while(handleQueueAndlistener(listener,queue, 1) > 0){ }
  if((read(playerA, &shipsA, sizeof(shipsA))) < 0){ perror("read"); resetGame(); return;}
  printf("Ship A placements: %d , %d\n", ntohl(shipsA.S1), ntohl(shipsA.S2));
  
  while(handleQueueAndlistener(listener,queue, 2) > 0){ }
  if((read(playerB, &shipsB, sizeof(shipsB))) < 0){ perror("read"); resetGame(); return;}
  //shipsB.S1 = 
  //shipsB.S2 = ;
  printf("Ship B placements: %d , %d\n", ntohl(shipsB.S1),  ntohl(shipsB.S2));

  //handleQueueAndlistener(listener,queue, 0);
  if (write(playerA, (void *)&shipsB, sizeof(shipsB)) < 0){ fprintf(stderr, "-1\n");resetGame(); return;}
  if (write(playerB, (void *)&shipsA, sizeof(shipsA)) < 0){fprintf(stderr, "-1\n");resetGame(); return;}
  



  int bufA = 4, bufB = 4;
  char  *shotA = (char *)malloc(bufA);
  char  *shotB = (char *)malloc(bufB);
  memset(shotA, 0, bufA);

  while(1){

    while(handleQueueAndlistener(listener,queue, 1) > 0){ }
    if((getword(playerA,shotA,&bufA)) < 0){ perror("read"); break;}
    printf("Player A shot square: %s \n", shotA);
    writealls(playerB, shotA);
    if((getword(playerA,shotA,&bufA)) < 0){ perror("read"); break;}
    printf("Plater A won?: %s\n", shotA);
    if(!strcmp(shotA, "1")){break;}
    
    while(handleQueueAndlistener(listener,queue, 2) > 0){ }
    if((getword(playerB,shotB,&bufB)) < 0){ perror("read"); break;}
    printf("Player B shot square: %s \n", shotB);
    writealls(playerA, shotB);
    if((getword(playerB,shotB,&bufB)) < 0){ perror("read"); break;}
    printf("player B won?: %s\n", shotB);
    if(!strcmp(shotB, "1")){break;}

    handleQueueAndlistener(listener,queue, 0);

    if(*shotA == '\0'){resetGame(); return;}
    if(*shotB == '\0'){resetGame(); return;}
    memset(shotA, 0, bufA);
    memset(shotB, 0, bufB);

  }



  
  
  resetGame();

}

void handleListener(int listener, int queue){
  struct sockaddr_in peer;
  socklen_t length;
  int extra;
  char msg[11];

  memset(msg, 0, sizeof(msg));
  length = sizeof(peer);
  switch(players){

    case 0: 
              
              if ((playerA =accept(listener, (struct sockaddr *)&peer, &length)) < 0) {
                  perror("handleListener:accept");
                  exit(1);
              }

              if((read(playerA, msg, sizeof(msg))) < 0){ perror("read"); break;}


              if(pflag){
                  printf("Clinet pass: %s\n", msg);
                  if( (*msg == '\0') || !!strcmp(msg, pass) ){
                    writealls(playerA, "F");
                    close(playerA);
                    playerA = 0;
                    break;
                  }
              }


              writeall(playerA, "A");
              while(handleQueueAndlistener(0,queue, 1) > 0){ }
              if((read(playerA, handleA, sizeof(handleA))) < 0){ perror("read"); exit(1);}
              if(*handleA == '\0'){ close(playerA); playerA = 0; break;}
              players++;
              break;
    case 1:  
              
              if ((playerB =accept(listener, (struct sockaddr *)&peer, &length)) < 0) {
                  perror("handleListener:accept");
                  exit(1);
              }

              if((read(playerB, msg, sizeof(msg))) < 0){ perror("read"); exit(1);}


              if(pflag){
                  printf("Clinet pass: %s\n", msg);
                  if( (*msg == '\0') || !!strcmp(msg, pass) ){
                    writealls(playerB, "F");
                    close(playerB);
                    playerB = 0;
                    break;
                  }
              }

              writeall(playerB, "B");
              while(handleQueueAndlistener(0,queue, 2) > 0){ }
              if((read(playerB, handleB, sizeof(handleB))) < 0){ perror("read"); exit(1);}
              players++;
              break;
    default:
            if ((extra =accept(listener, (struct sockaddr *)&peer, &length)) < 0) {
                  perror("handleListener:accept");
                  exit(1);
              }
            writeall(extra, "f");
            close(extra);
  }
}

void handleQueue(int queue_sock){
  int cc, len;
  struct sockaddr_in  from;
  socklen_t fsize;
  char msg[11];
  memset(msg, 0, sizeof(msg));
  fsize = sizeof(from);

  memset(msg, 0, sizeof(msg));
  cc = recvfrom(queue_sock, msg, sizeof(msg),0, (struct sockaddr *)&from, &fsize);
  if (cc < 0) perror("bigbs/handleQueue:recvfrom");
  
  len = sizeof(msg);
  if(pflag){
    
    if(!!strcmp(msg,pass)){
        memset(msg, 0, sizeof(msg));
        strncpy(msg,"F",2);
        if(sendall(queue_sock, msg, &len,  (struct sockaddr *)&from, fsize) < 0){
          perror("bigbs/handleQueue:sendall");
          fprintf(stderr,"We only sent %d bytes because of the error!\n", len);
          exit(1); 
        }
      return;
    }

  }

  //if(strncmp(msg,"q", 2) != 0){
    //fprintf(stderr,"recvfrom query 1 not right\n");
    //exit(1);
  //}

  memset(msg, 0, sizeof(msg));
  len = sizeof(msg);

  if(players == 0){
    //if(sprintf(msg,"%d", players) < 0 ){ perror("bigbs/handleQueue:recvfrom"); exit(1);}
    //source:http://beej.us/guide/bgnet/output/html/multipage/advanced.html#sendall
    
    strncpy(msg,"0",2);
    if(sendall(queue_sock, msg, &len,  (struct sockaddr *)&from, fsize) < 0){
      perror("bigbs/handleQueue:sendall");
      fprintf(stderr,"We only sent %d bytes because of the error!\n", len);
      exit(1); 
    }

  }
  else if( players == 1){

    strcpy(msg,handleA);
    if(sendall(queue_sock, msg, &len,  (struct sockaddr *)&from, fsize) < 0){
      perror("bigbs/handleQueue:sendall");
      fprintf(stderr,"We only sent %d bytes because of the error!\n", len);
      exit(1); 
    }
    

  }
  else if(players == 2){
    strncpy(msg,"2",2);
    len = strlen(msg) + 1;
    if(sendall(queue_sock, msg, &len, (struct sockaddr *)&from, fsize) < 0){
      perror("bigbs/handleQueue:sendall");
      fprintf(stderr,"We only sent %d bytes because of the error!\n", len);
      exit(1); 
    }

  }
  else{
    fprintf(stderr,"players = %d\n", players );
    exit(1);
  }


}

int handleQueueAndlistener(int listener, int queue, int flag){


    
    fd_set master, read;
    int i,maxfd, max, A = 1, B = 1;

    
    FD_ZERO(&master);
    FD_ZERO(&read);
  
    FD_SET(listener,&master);
    FD_SET(queue,&master);

    FD_SET(playerA,&master);
    FD_SET(playerB,&master);
    max = MAX(playerA,playerB);
    

    /*if(flag == 0){
      A = 0;
      B = 0;
      max = 0;
    }
    else if(flag == 1){
      FD_SET(playerA,&master);
      FD_SET(playerB,&master);
      max = MAX(playerA,playerB);
    }
    else{
        FD_SET(playerA,&master);
        FD_SET(playerB,&master);
        max = MAX(playerA,playerB);
        A = 0;
    }*/
   


    
    
    read = master;
    
    

    maxfd = MAX(MAX(listener, queue), max);
    if((select(maxfd+1, &read, NULL, NULL, &timeout)) == -1){
      perror("bigbs:select");
      exit(1);
    }

    //puts("after select");
    for(i = 0; i <=maxfd; i++){
        if(FD_ISSET(i, &read)){
          
          if(i == queue){
            handleQueue(queue);
          }
          else if( (i == listener) &&  (listener != 0)){

            handleListener(listener, queue); 

          }
          else if((i == playerA) && A && (playerA != 0) ){ //player A is ready
            A = 0;
          }
          else if((i == playerB) && B &&  (playerB != 0)   ){  //Player B is ready
            
            B = 0;

          }
        
        }

          
    }
  
  //if(flag){printf("%d %d\n",A, B );}
  if((flag == 1) && !A){return -1;}       //A is ready to read
  if((flag == 2) && !B){return -1;}       //B is ready to read
  if((flag == 3) && !B && !A){return -1;} // A and B are ready to read 


  return 1;
}

int main(int argc, char* argv[]){
	socklen_t len_s,len_q;
	struct sockaddr_in *localaddr, * queueaddr;
	int listener, queue;


  if(argc > 2){
    print_usage();
    exit(0);
  }

  if(argc == 2){
    pflag = 1;
    if(strlen(argv[1]) > 10){
      puts("Password is at most 10 chars");
      exit(0);
    }
    strcpy(pass,argv[1]);
    printf("pass %s\n", pass);
  }
  
 

  
 
	
  setupSock(&queueaddr, &queue, SOCK_DGRAM, NULL, "0");
  setupSock(&localaddr, &listener, SOCK_STREAM, NULL, "0");
	

	if (bind(listener, (struct sockaddr *)localaddr, sizeof(struct sockaddr_in)) < 0) {
    	perror("bigbs:bind");
    	exit(1);
    }

  if (bind(queue, (struct sockaddr *)queueaddr, sizeof(struct sockaddr_in)) < 0) {
    	perror("bigbs:bind");
    	exit(1);
  }

  //printServerInfo( localaddr, &listener, &len_s, &len_q);
  printServerInfo( localaddr, queueaddr, &listener, &queue, &len_s,&len_q);

  if(listen(listener, 3)== -1){
      perror("bigbs:listener");
      exit(1);}


  fflush(stdout);
  timeout.tv_sec = 0;
  timeout.tv_usec = 0;
  memset(handleA, 0 , sizeof(handleA));
  memset(handleB, 0 , sizeof(handleB));
  signal(SIGHUP, termsighander);
  //resetGame();
  for(;;){
    //write = master;

    
    handleQueueAndlistener(listener,queue, 0);

    
    if(players == 2){
      startGame( listener,queue);
      puts("out game");
    }


  }

  return 0;
}



	

